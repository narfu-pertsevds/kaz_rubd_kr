defmodule KazRubdKr.Repo.Migrations.CreateStaff do
  use Ecto.Migration

  def change do
    create table(:staff) do
      add :full_name, :string

      timestamps(type: :utc_datetime)
    end
  end
end
