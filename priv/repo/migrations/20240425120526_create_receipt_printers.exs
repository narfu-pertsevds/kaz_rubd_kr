defmodule KazRubdKr.Repo.Migrations.CreateReceiptPrinters do
  use Ecto.Migration

  def change do
    create table(:receipt_printers) do
      add :name, :string
      add :connection_details, :string

      timestamps(type: :utc_datetime)
    end
  end
end
