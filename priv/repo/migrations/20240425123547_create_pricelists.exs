defmodule KazRubdKr.Repo.Migrations.CreatePricelists do
  use Ecto.Migration

  def change do
    create table(:pricelists) do
      add :date, :utc_datetime
      add :staff_id, references(:staff, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:pricelists, [:staff_id])
    many_to_many :products, KazRubdKr.Sales.Product, join_through: "products_in_pricelists"
  end
end
