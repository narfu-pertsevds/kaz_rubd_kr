defmodule KazRubdKr.Repo.Migrations.CreateProductsInPricelists do
  use Ecto.Migration

  def change do
    create table(:products_in_pricelists) do
      add :price, :decimal
      add :pricelists_id, references(:pricelists, on_delete: :nothing)
      add :produtcs_id, references(:products, on_delete: :nothing)
      add :units_id, references(:units, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:products_in_pricelists, [:pricelists_id])
    create index(:products_in_pricelists, [:produtcs_id])
    create index(:products_in_pricelists, [:units_id])
  end
end
