defmodule KazRubdKr.Repo.Migrations.CreateProductsInReceipts do
  use Ecto.Migration

  def change do
    create table(:products_in_receipts) do
      add :receipts_id, references(:receipts, on_delete: :nothing)
      add :produtcs_id, references(:products, on_delete: :nothing)
      add :units_id, references(:units, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:products_in_receipts, [:receipts_id])
    create index(:products_in_receipts, [:produtcs_id])
    create index(:products_in_receipts, [:units_id])
  end
end
