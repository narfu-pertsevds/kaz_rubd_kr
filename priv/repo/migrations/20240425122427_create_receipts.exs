defmodule KazRubdKr.Repo.Migrations.CreateReceipts do
  use Ecto.Migration

  def change do
    create table(:receipts) do
      add :date, :utc_datetime
      add :total_price, :decimal
      add :staff_id, references(:staff, on_delete: :nothing)
      add :printer_id, references(:receipt_printers, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:receipts, [:staff_id])
    create index(:receipts, [:printer_id])
    many_to_many :products, KazRubdKr.Sales.Product, join_through: "products_in_receipts"
  end
end
