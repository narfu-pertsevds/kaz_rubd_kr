defmodule KazRubdKr.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :code, :string

      timestamps(type: :utc_datetime)
    end
    many_to_many :receipts, KazRubdKr.Sales.Receipt, join_through: "products_in_receipts"
    many_to_many :pricelists, KazRubdKr.Sales.Pricelist, join_through: "products_in_pricelists"
  end
end
