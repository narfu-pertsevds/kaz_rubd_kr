defmodule KazRubdKr.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      KazRubdKrWeb.Telemetry,
      KazRubdKr.Repo,
      {DNSCluster, query: Application.get_env(:kaz_rubd_kr, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: KazRubdKr.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: KazRubdKr.Finch},
      # Start a worker by calling: KazRubdKr.Worker.start_link(arg)
      # {KazRubdKr.Worker, arg},
      # Start to serve requests, typically the last entry
      KazRubdKrWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: KazRubdKr.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    KazRubdKrWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
