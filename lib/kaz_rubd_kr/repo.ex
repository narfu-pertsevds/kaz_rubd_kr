defmodule KazRubdKr.Repo do
  use Ecto.Repo,
    otp_app: :kaz_rubd_kr,
    adapter: Ecto.Adapters.Postgres
end
