defmodule KazRubdKr.Sales do
  @moduledoc """
  The Sales context.
  """

  import Ecto.Query, warn: false
  alias KazRubdKr.Repo

  alias KazRubdKr.Sales.Staff

  @doc """
  Returns the list of staff.

  ## Examples

      iex> list_staff()
      [%Staff{}, ...]

  """
  def list_staff do
    Repo.all(Staff)
  end

  @doc """
  Gets a single staff.

  Raises `Ecto.NoResultsError` if the Staff does not exist.

  ## Examples

      iex> get_staff!(123)
      %Staff{}

      iex> get_staff!(456)
      ** (Ecto.NoResultsError)

  """
  def get_staff!(id), do: Repo.get!(Staff, id)

  @doc """
  Creates a staff.

  ## Examples

      iex> create_staff(%{field: value})
      {:ok, %Staff{}}

      iex> create_staff(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_staff(attrs \\ %{}) do
    %Staff{}
    |> Staff.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a staff.

  ## Examples

      iex> update_staff(staff, %{field: new_value})
      {:ok, %Staff{}}

      iex> update_staff(staff, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_staff(%Staff{} = staff, attrs) do
    staff
    |> Staff.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a staff.

  ## Examples

      iex> delete_staff(staff)
      {:ok, %Staff{}}

      iex> delete_staff(staff)
      {:error, %Ecto.Changeset{}}

  """
  def delete_staff(%Staff{} = staff) do
    Repo.delete(staff)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking staff changes.

  ## Examples

      iex> change_staff(staff)
      %Ecto.Changeset{data: %Staff{}}

  """
  def change_staff(%Staff{} = staff, attrs \\ %{}) do
    Staff.changeset(staff, attrs)
  end

  alias KazRubdKr.Sales.Receipt_printer

  @doc """
  Returns the list of receipt_printers.

  ## Examples

      iex> list_receipt_printers()
      [%Receipt_printer{}, ...]

  """
  def list_receipt_printers do
    Repo.all(Receipt_printer)
  end

  @doc """
  Gets a single receipt_printer.

  Raises `Ecto.NoResultsError` if the Receipt printer does not exist.

  ## Examples

      iex> get_receipt_printer!(123)
      %Receipt_printer{}

      iex> get_receipt_printer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_receipt_printer!(id), do: Repo.get!(Receipt_printer, id)

  @doc """
  Creates a receipt_printer.

  ## Examples

      iex> create_receipt_printer(%{field: value})
      {:ok, %Receipt_printer{}}

      iex> create_receipt_printer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_receipt_printer(attrs \\ %{}) do
    %Receipt_printer{}
    |> Receipt_printer.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a receipt_printer.

  ## Examples

      iex> update_receipt_printer(receipt_printer, %{field: new_value})
      {:ok, %Receipt_printer{}}

      iex> update_receipt_printer(receipt_printer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_receipt_printer(%Receipt_printer{} = receipt_printer, attrs) do
    receipt_printer
    |> Receipt_printer.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a receipt_printer.

  ## Examples

      iex> delete_receipt_printer(receipt_printer)
      {:ok, %Receipt_printer{}}

      iex> delete_receipt_printer(receipt_printer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_receipt_printer(%Receipt_printer{} = receipt_printer) do
    Repo.delete(receipt_printer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking receipt_printer changes.

  ## Examples

      iex> change_receipt_printer(receipt_printer)
      %Ecto.Changeset{data: %Receipt_printer{}}

  """
  def change_receipt_printer(%Receipt_printer{} = receipt_printer, attrs \\ %{}) do
    Receipt_printer.changeset(receipt_printer, attrs)
  end

  alias KazRubdKr.Sales.Product

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products do
    Repo.all(Product)
  end

  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

      iex> get_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_product!(id), do: Repo.get!(Product, id)

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{data: %Product{}}

  """
  def change_product(%Product{} = product, attrs \\ %{}) do
    Product.changeset(product, attrs)
  end

  alias KazRubdKr.Sales.Unit

  @doc """
  Returns the list of units.

  ## Examples

      iex> list_units()
      [%Unit{}, ...]

  """
  def list_units do
    Repo.all(Unit)
  end

  @doc """
  Gets a single unit.

  Raises `Ecto.NoResultsError` if the Unit does not exist.

  ## Examples

      iex> get_unit!(123)
      %Unit{}

      iex> get_unit!(456)
      ** (Ecto.NoResultsError)

  """
  def get_unit!(id), do: Repo.get!(Unit, id)

  @doc """
  Creates a unit.

  ## Examples

      iex> create_unit(%{field: value})
      {:ok, %Unit{}}

      iex> create_unit(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_unit(attrs \\ %{}) do
    %Unit{}
    |> Unit.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a unit.

  ## Examples

      iex> update_unit(unit, %{field: new_value})
      {:ok, %Unit{}}

      iex> update_unit(unit, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_unit(%Unit{} = unit, attrs) do
    unit
    |> Unit.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a unit.

  ## Examples

      iex> delete_unit(unit)
      {:ok, %Unit{}}

      iex> delete_unit(unit)
      {:error, %Ecto.Changeset{}}

  """
  def delete_unit(%Unit{} = unit) do
    Repo.delete(unit)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking unit changes.

  ## Examples

      iex> change_unit(unit)
      %Ecto.Changeset{data: %Unit{}}

  """
  def change_unit(%Unit{} = unit, attrs \\ %{}) do
    Unit.changeset(unit, attrs)
  end

  alias KazRubdKr.Sales.Receipt

  @doc """
  Returns the list of receipts.

  ## Examples

      iex> list_receipts()
      [%Receipt{}, ...]

  """
  def list_receipts do
    Repo.all(Receipt)
  end

  @doc """
  Gets a single receipt.

  Raises `Ecto.NoResultsError` if the Receipt does not exist.

  ## Examples

      iex> get_receipt!(123)
      %Receipt{}

      iex> get_receipt!(456)
      ** (Ecto.NoResultsError)

  """
  def get_receipt!(id), do: Repo.get!(Receipt, id)

  @doc """
  Creates a receipt.

  ## Examples

      iex> create_receipt(%{field: value})
      {:ok, %Receipt{}}

      iex> create_receipt(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_receipt(attrs \\ %{}) do
    %Receipt{}
    |> Receipt.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a receipt.

  ## Examples

      iex> update_receipt(receipt, %{field: new_value})
      {:ok, %Receipt{}}

      iex> update_receipt(receipt, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_receipt(%Receipt{} = receipt, attrs) do
    receipt
    |> Receipt.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a receipt.

  ## Examples

      iex> delete_receipt(receipt)
      {:ok, %Receipt{}}

      iex> delete_receipt(receipt)
      {:error, %Ecto.Changeset{}}

  """
  def delete_receipt(%Receipt{} = receipt) do
    Repo.delete(receipt)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking receipt changes.

  ## Examples

      iex> change_receipt(receipt)
      %Ecto.Changeset{data: %Receipt{}}

  """
  def change_receipt(%Receipt{} = receipt, attrs \\ %{}) do
    Receipt.changeset(receipt, attrs)
  end

  alias KazRubdKr.Sales.Pricelist

  @doc """
  Returns the list of pricelists.

  ## Examples

      iex> list_pricelists()
      [%Pricelist{}, ...]

  """
  def list_pricelists do
    Repo.all(Pricelist)
  end

  @doc """
  Gets a single pricelist.

  Raises `Ecto.NoResultsError` if the Pricelist does not exist.

  ## Examples

      iex> get_pricelist!(123)
      %Pricelist{}

      iex> get_pricelist!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pricelist!(id), do: Repo.get!(Pricelist, id)

  @doc """
  Creates a pricelist.

  ## Examples

      iex> create_pricelist(%{field: value})
      {:ok, %Pricelist{}}

      iex> create_pricelist(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pricelist(attrs \\ %{}) do
    %Pricelist{}
    |> Pricelist.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pricelist.

  ## Examples

      iex> update_pricelist(pricelist, %{field: new_value})
      {:ok, %Pricelist{}}

      iex> update_pricelist(pricelist, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pricelist(%Pricelist{} = pricelist, attrs) do
    pricelist
    |> Pricelist.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a pricelist.

  ## Examples

      iex> delete_pricelist(pricelist)
      {:ok, %Pricelist{}}

      iex> delete_pricelist(pricelist)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pricelist(%Pricelist{} = pricelist) do
    Repo.delete(pricelist)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pricelist changes.

  ## Examples

      iex> change_pricelist(pricelist)
      %Ecto.Changeset{data: %Pricelist{}}

  """
  def change_pricelist(%Pricelist{} = pricelist, attrs \\ %{}) do
    Pricelist.changeset(pricelist, attrs)
  end
end
