defmodule KazRubdKr.ProductsInReceipts do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products_in_receipts" do

    field :receipts_id, :id
    field :produtcs_id, :id
    field :units_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(products_in_receipts, attrs) do
    products_in_receipts
    |> cast(attrs, [])
    |> validate_required([])
  end
end
