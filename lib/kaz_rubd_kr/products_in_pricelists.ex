defmodule KazRubdKr.ProductsInPricelists do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products_in_pricelists" do
    field :price, :decimal
    field :pricelists_id, :id
    field :produtcs_id, :id
    field :units_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(products_in_pricelists, attrs) do
    products_in_pricelists
    |> cast(attrs, [:price])
    |> validate_required([:price])
  end
end
