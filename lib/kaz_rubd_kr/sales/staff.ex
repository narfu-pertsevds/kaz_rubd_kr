defmodule KazRubdKr.Sales.Staff do
  use Ecto.Schema
  import Ecto.Changeset

  schema "staff" do
    field :full_name, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(staff, attrs) do
    staff
    |> cast(attrs, [:full_name])
    |> validate_required([:full_name])
  end
end
