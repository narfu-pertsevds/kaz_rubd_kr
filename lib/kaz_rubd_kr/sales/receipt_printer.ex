defmodule KazRubdKr.Sales.Receipt_printer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "receipt_printers" do
    field :name, :string
    field :connection_details, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(receipt_printer, attrs) do
    receipt_printer
    |> cast(attrs, [:name, :connection_details])
    |> validate_required([:name, :connection_details])
  end
end
