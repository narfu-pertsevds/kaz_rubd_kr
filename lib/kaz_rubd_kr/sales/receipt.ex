defmodule KazRubdKr.Sales.Receipt do
  use Ecto.Schema
  import Ecto.Changeset

  schema "receipts" do
    field :date, :utc_datetime
    field :total_price, :decimal
    field :staff_id, :id
    field :printer_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(receipt, attrs) do
    receipt
    |> cast(attrs, [:date, :total_price])
    |> validate_required([:date, :total_price])
  end
end
