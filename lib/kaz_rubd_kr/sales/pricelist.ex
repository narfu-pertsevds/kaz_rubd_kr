defmodule KazRubdKr.Sales.Pricelist do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pricelists" do
    field :date, :utc_datetime
    field :staff_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(pricelist, attrs) do
    pricelist
    |> cast(attrs, [:date])
    |> validate_required([:date])
  end
end
