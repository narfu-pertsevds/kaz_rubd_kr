defmodule KazRubdKrWeb.Router do
  use KazRubdKrWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {KazRubdKrWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", KazRubdKrWeb do
    pipe_through :browser

    get "/", PageController, :home

    live "/staff", StaffLive.Index, :index
    live "/staff/new", StaffLive.Index, :new
    live "/staff/:id/edit", StaffLive.Index, :edit

    live "/staff/:id", StaffLive.Show, :show
    live "/staff/:id/show/edit", StaffLive.Show, :edit

    live "/receipt_printers", Receipt_printerLive.Index, :index
    live "/receipt_printers/new", Receipt_printerLive.Index, :new
    live "/receipt_printers/:id/edit", Receipt_printerLive.Index, :edit

    live "/receipt_printers/:id", Receipt_printerLive.Show, :show
    live "/receipt_printers/:id/show/edit", Receipt_printerLive.Show, :edit

    live "/products", ProductLive.Index, :index
    live "/products/new", ProductLive.Index, :new
    live "/products/:id/edit", ProductLive.Index, :edit

    live "/products/:id", ProductLive.Show, :show
    live "/products/:id/show/edit", ProductLive.Show, :edit

    live "/units", UnitLive.Index, :index
    live "/units/new", UnitLive.Index, :new
    live "/units/:id/edit", UnitLive.Index, :edit

    live "/units/:id", UnitLive.Show, :show
    live "/units/:id/show/edit", UnitLive.Show, :edit

    live "/receipts", ReceiptLive.Index, :index
    live "/receipts/new", ReceiptLive.Index, :new
    live "/receipts/:id/edit", ReceiptLive.Index, :edit

    live "/receipts/:id", ReceiptLive.Show, :show
    live "/receipts/:id/show/edit", ReceiptLive.Show, :edit

    live "/pricelists", PricelistLive.Index, :index
    live "/pricelists/new", PricelistLive.Index, :new
    live "/pricelists/:id/edit", PricelistLive.Index, :edit

    live "/pricelists/:id", PricelistLive.Show, :show
    live "/pricelists/:id/show/edit", PricelistLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  # scope "/api", KazRubdKrWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:kaz_rubd_kr, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: KazRubdKrWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
