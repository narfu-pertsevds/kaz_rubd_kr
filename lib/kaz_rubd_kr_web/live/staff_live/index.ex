defmodule KazRubdKrWeb.StaffLive.Index do
  use KazRubdKrWeb, :live_view

  alias KazRubdKr.Sales
  alias KazRubdKr.Sales.Staff

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :staff_collection, Sales.list_staff())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Staff")
    |> assign(:staff, Sales.get_staff!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Staff")
    |> assign(:staff, %Staff{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Staff")
    |> assign(:staff, nil)
  end

  @impl true
  def handle_info({KazRubdKrWeb.StaffLive.FormComponent, {:saved, staff}}, socket) do
    {:noreply, stream_insert(socket, :staff_collection, staff)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    staff = Sales.get_staff!(id)
    {:ok, _} = Sales.delete_staff(staff)

    {:noreply, stream_delete(socket, :staff_collection, staff)}
  end
end
