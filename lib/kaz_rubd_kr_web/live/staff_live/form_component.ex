defmodule KazRubdKrWeb.StaffLive.FormComponent do
  use KazRubdKrWeb, :live_component

  alias KazRubdKr.Sales

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage staff records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="staff-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:full_name]} type="text" label="Full name" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Staff</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{staff: staff} = assigns, socket) do
    changeset = Sales.change_staff(staff)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"staff" => staff_params}, socket) do
    changeset =
      socket.assigns.staff
      |> Sales.change_staff(staff_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"staff" => staff_params}, socket) do
    save_staff(socket, socket.assigns.action, staff_params)
  end

  defp save_staff(socket, :edit, staff_params) do
    case Sales.update_staff(socket.assigns.staff, staff_params) do
      {:ok, staff} ->
        notify_parent({:saved, staff})

        {:noreply,
         socket
         |> put_flash(:info, "Staff updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_staff(socket, :new, staff_params) do
    case Sales.create_staff(staff_params) do
      {:ok, staff} ->
        notify_parent({:saved, staff})

        {:noreply,
         socket
         |> put_flash(:info, "Staff created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
