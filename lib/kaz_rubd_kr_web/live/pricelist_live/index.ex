defmodule KazRubdKrWeb.PricelistLive.Index do
  use KazRubdKrWeb, :live_view

  alias KazRubdKr.Sales
  alias KazRubdKr.Sales.Pricelist

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :pricelists, Sales.list_pricelists())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Pricelist")
    |> assign(:pricelist, Sales.get_pricelist!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Pricelist")
    |> assign(:pricelist, %Pricelist{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Pricelists")
    |> assign(:pricelist, nil)
  end

  @impl true
  def handle_info({KazRubdKrWeb.PricelistLive.FormComponent, {:saved, pricelist}}, socket) do
    {:noreply, stream_insert(socket, :pricelists, pricelist)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    pricelist = Sales.get_pricelist!(id)
    {:ok, _} = Sales.delete_pricelist(pricelist)

    {:noreply, stream_delete(socket, :pricelists, pricelist)}
  end
end
