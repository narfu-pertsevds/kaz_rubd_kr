defmodule KazRubdKrWeb.PricelistLive.FormComponent do
  use KazRubdKrWeb, :live_component

  alias KazRubdKr.Sales

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage pricelist records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="pricelist-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:date]} type="datetime-local" label="Date" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Pricelist</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{pricelist: pricelist} = assigns, socket) do
    changeset = Sales.change_pricelist(pricelist)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"pricelist" => pricelist_params}, socket) do
    changeset =
      socket.assigns.pricelist
      |> Sales.change_pricelist(pricelist_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"pricelist" => pricelist_params}, socket) do
    save_pricelist(socket, socket.assigns.action, pricelist_params)
  end

  defp save_pricelist(socket, :edit, pricelist_params) do
    case Sales.update_pricelist(socket.assigns.pricelist, pricelist_params) do
      {:ok, pricelist} ->
        notify_parent({:saved, pricelist})

        {:noreply,
         socket
         |> put_flash(:info, "Pricelist updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_pricelist(socket, :new, pricelist_params) do
    case Sales.create_pricelist(pricelist_params) do
      {:ok, pricelist} ->
        notify_parent({:saved, pricelist})

        {:noreply,
         socket
         |> put_flash(:info, "Pricelist created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
