defmodule KazRubdKrWeb.PricelistLive.Show do
  use KazRubdKrWeb, :live_view

  alias KazRubdKr.Sales

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:pricelist, Sales.get_pricelist!(id))}
  end

  defp page_title(:show), do: "Show Pricelist"
  defp page_title(:edit), do: "Edit Pricelist"
end
