defmodule KazRubdKrWeb.ReceiptLive.Index do
  use KazRubdKrWeb, :live_view

  alias KazRubdKr.Sales
  alias KazRubdKr.Sales.Receipt

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :receipts, Sales.list_receipts())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Receipt")
    |> assign(:receipt, Sales.get_receipt!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Receipt")
    |> assign(:receipt, %Receipt{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Receipts")
    |> assign(:receipt, nil)
  end

  @impl true
  def handle_info({KazRubdKrWeb.ReceiptLive.FormComponent, {:saved, receipt}}, socket) do
    {:noreply, stream_insert(socket, :receipts, receipt)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    receipt = Sales.get_receipt!(id)
    {:ok, _} = Sales.delete_receipt(receipt)

    {:noreply, stream_delete(socket, :receipts, receipt)}
  end
end
