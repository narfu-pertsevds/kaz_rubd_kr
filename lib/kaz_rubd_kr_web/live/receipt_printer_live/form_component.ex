defmodule KazRubdKrWeb.Receipt_printerLive.FormComponent do
  use KazRubdKrWeb, :live_component

  alias KazRubdKr.Sales

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage receipt_printer records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="receipt_printer-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:connection_details]} type="text" label="Connection details" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Receipt printer</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{receipt_printer: receipt_printer} = assigns, socket) do
    changeset = Sales.change_receipt_printer(receipt_printer)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"receipt_printer" => receipt_printer_params}, socket) do
    changeset =
      socket.assigns.receipt_printer
      |> Sales.change_receipt_printer(receipt_printer_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"receipt_printer" => receipt_printer_params}, socket) do
    save_receipt_printer(socket, socket.assigns.action, receipt_printer_params)
  end

  defp save_receipt_printer(socket, :edit, receipt_printer_params) do
    case Sales.update_receipt_printer(socket.assigns.receipt_printer, receipt_printer_params) do
      {:ok, receipt_printer} ->
        notify_parent({:saved, receipt_printer})

        {:noreply,
         socket
         |> put_flash(:info, "Receipt printer updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_receipt_printer(socket, :new, receipt_printer_params) do
    case Sales.create_receipt_printer(receipt_printer_params) do
      {:ok, receipt_printer} ->
        notify_parent({:saved, receipt_printer})

        {:noreply,
         socket
         |> put_flash(:info, "Receipt printer created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
