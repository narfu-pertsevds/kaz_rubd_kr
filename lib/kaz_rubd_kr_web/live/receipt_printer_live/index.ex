defmodule KazRubdKrWeb.Receipt_printerLive.Index do
  use KazRubdKrWeb, :live_view

  alias KazRubdKr.Sales
  alias KazRubdKr.Sales.Receipt_printer

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :receipt_printers, Sales.list_receipt_printers())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Receipt printer")
    |> assign(:receipt_printer, Sales.get_receipt_printer!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Receipt printer")
    |> assign(:receipt_printer, %Receipt_printer{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Receipt printers")
    |> assign(:receipt_printer, nil)
  end

  @impl true
  def handle_info({KazRubdKrWeb.Receipt_printerLive.FormComponent, {:saved, receipt_printer}}, socket) do
    {:noreply, stream_insert(socket, :receipt_printers, receipt_printer)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    receipt_printer = Sales.get_receipt_printer!(id)
    {:ok, _} = Sales.delete_receipt_printer(receipt_printer)

    {:noreply, stream_delete(socket, :receipt_printers, receipt_printer)}
  end
end
