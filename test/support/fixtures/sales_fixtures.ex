defmodule KazRubdKr.SalesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `KazRubdKr.Sales` context.
  """

  @doc """
  Generate a staff.
  """
  def staff_fixture(attrs \\ %{}) do
    {:ok, staff} =
      attrs
      |> Enum.into(%{
        full_name: "some full_name"
      })
      |> KazRubdKr.Sales.create_staff()

    staff
  end

  @doc """
  Generate a receipt_printer.
  """
  def receipt_printer_fixture(attrs \\ %{}) do
    {:ok, receipt_printer} =
      attrs
      |> Enum.into(%{
        connection_details: "some connection_details",
        name: "some name"
      })
      |> KazRubdKr.Sales.create_receipt_printer()

    receipt_printer
  end

  @doc """
  Generate a product.
  """
  def product_fixture(attrs \\ %{}) do
    {:ok, product} =
      attrs
      |> Enum.into(%{
        code: "some code",
        name: "some name"
      })
      |> KazRubdKr.Sales.create_product()

    product
  end

  @doc """
  Generate a unit.
  """
  def unit_fixture(attrs \\ %{}) do
    {:ok, unit} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> KazRubdKr.Sales.create_unit()

    unit
  end

  @doc """
  Generate a receipt.
  """
  def receipt_fixture(attrs \\ %{}) do
    {:ok, receipt} =
      attrs
      |> Enum.into(%{
        date: ~U[2024-04-24 12:24:00Z],
        total_price: "120.5"
      })
      |> KazRubdKr.Sales.create_receipt()

    receipt
  end

  @doc """
  Generate a pricelist.
  """
  def pricelist_fixture(attrs \\ %{}) do
    {:ok, pricelist} =
      attrs
      |> Enum.into(%{
        date: ~U[2024-04-24 12:35:00Z]
      })
      |> KazRubdKr.Sales.create_pricelist()

    pricelist
  end
end
