defmodule KazRubdKr.SalesTest do
  use KazRubdKr.DataCase

  alias KazRubdKr.Sales

  describe "staff" do
    alias KazRubdKr.Sales.Staff

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{full_name: nil}

    test "list_staff/0 returns all staff" do
      staff = staff_fixture()
      assert Sales.list_staff() == [staff]
    end

    test "get_staff!/1 returns the staff with given id" do
      staff = staff_fixture()
      assert Sales.get_staff!(staff.id) == staff
    end

    test "create_staff/1 with valid data creates a staff" do
      valid_attrs = %{full_name: "some full_name"}

      assert {:ok, %Staff{} = staff} = Sales.create_staff(valid_attrs)
      assert staff.full_name == "some full_name"
    end

    test "create_staff/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_staff(@invalid_attrs)
    end

    test "update_staff/2 with valid data updates the staff" do
      staff = staff_fixture()
      update_attrs = %{full_name: "some updated full_name"}

      assert {:ok, %Staff{} = staff} = Sales.update_staff(staff, update_attrs)
      assert staff.full_name == "some updated full_name"
    end

    test "update_staff/2 with invalid data returns error changeset" do
      staff = staff_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_staff(staff, @invalid_attrs)
      assert staff == Sales.get_staff!(staff.id)
    end

    test "delete_staff/1 deletes the staff" do
      staff = staff_fixture()
      assert {:ok, %Staff{}} = Sales.delete_staff(staff)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_staff!(staff.id) end
    end

    test "change_staff/1 returns a staff changeset" do
      staff = staff_fixture()
      assert %Ecto.Changeset{} = Sales.change_staff(staff)
    end
  end

  describe "receipt_printers" do
    alias KazRubdKr.Sales.Receipt_printer

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{name: nil, connection_details: nil}

    test "list_receipt_printers/0 returns all receipt_printers" do
      receipt_printer = receipt_printer_fixture()
      assert Sales.list_receipt_printers() == [receipt_printer]
    end

    test "get_receipt_printer!/1 returns the receipt_printer with given id" do
      receipt_printer = receipt_printer_fixture()
      assert Sales.get_receipt_printer!(receipt_printer.id) == receipt_printer
    end

    test "create_receipt_printer/1 with valid data creates a receipt_printer" do
      valid_attrs = %{name: "some name", connection_details: "some connection_details"}

      assert {:ok, %Receipt_printer{} = receipt_printer} = Sales.create_receipt_printer(valid_attrs)
      assert receipt_printer.name == "some name"
      assert receipt_printer.connection_details == "some connection_details"
    end

    test "create_receipt_printer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_receipt_printer(@invalid_attrs)
    end

    test "update_receipt_printer/2 with valid data updates the receipt_printer" do
      receipt_printer = receipt_printer_fixture()
      update_attrs = %{name: "some updated name", connection_details: "some updated connection_details"}

      assert {:ok, %Receipt_printer{} = receipt_printer} = Sales.update_receipt_printer(receipt_printer, update_attrs)
      assert receipt_printer.name == "some updated name"
      assert receipt_printer.connection_details == "some updated connection_details"
    end

    test "update_receipt_printer/2 with invalid data returns error changeset" do
      receipt_printer = receipt_printer_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_receipt_printer(receipt_printer, @invalid_attrs)
      assert receipt_printer == Sales.get_receipt_printer!(receipt_printer.id)
    end

    test "delete_receipt_printer/1 deletes the receipt_printer" do
      receipt_printer = receipt_printer_fixture()
      assert {:ok, %Receipt_printer{}} = Sales.delete_receipt_printer(receipt_printer)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_receipt_printer!(receipt_printer.id) end
    end

    test "change_receipt_printer/1 returns a receipt_printer changeset" do
      receipt_printer = receipt_printer_fixture()
      assert %Ecto.Changeset{} = Sales.change_receipt_printer(receipt_printer)
    end
  end

  describe "products" do
    alias KazRubdKr.Sales.Product

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{code: nil, name: nil}

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Sales.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Sales.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      valid_attrs = %{code: "some code", name: "some name"}

      assert {:ok, %Product{} = product} = Sales.create_product(valid_attrs)
      assert product.code == "some code"
      assert product.name == "some name"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      update_attrs = %{code: "some updated code", name: "some updated name"}

      assert {:ok, %Product{} = product} = Sales.update_product(product, update_attrs)
      assert product.code == "some updated code"
      assert product.name == "some updated name"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_product(product, @invalid_attrs)
      assert product == Sales.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Sales.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Sales.change_product(product)
    end
  end

  describe "units" do
    alias KazRubdKr.Sales.Unit

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{name: nil}

    test "list_units/0 returns all units" do
      unit = unit_fixture()
      assert Sales.list_units() == [unit]
    end

    test "get_unit!/1 returns the unit with given id" do
      unit = unit_fixture()
      assert Sales.get_unit!(unit.id) == unit
    end

    test "create_unit/1 with valid data creates a unit" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Unit{} = unit} = Sales.create_unit(valid_attrs)
      assert unit.name == "some name"
    end

    test "create_unit/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_unit(@invalid_attrs)
    end

    test "update_unit/2 with valid data updates the unit" do
      unit = unit_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Unit{} = unit} = Sales.update_unit(unit, update_attrs)
      assert unit.name == "some updated name"
    end

    test "update_unit/2 with invalid data returns error changeset" do
      unit = unit_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_unit(unit, @invalid_attrs)
      assert unit == Sales.get_unit!(unit.id)
    end

    test "delete_unit/1 deletes the unit" do
      unit = unit_fixture()
      assert {:ok, %Unit{}} = Sales.delete_unit(unit)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_unit!(unit.id) end
    end

    test "change_unit/1 returns a unit changeset" do
      unit = unit_fixture()
      assert %Ecto.Changeset{} = Sales.change_unit(unit)
    end
  end

  describe "receipts" do
    alias KazRubdKr.Sales.Receipt

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{date: nil, total_price: nil}

    test "list_receipts/0 returns all receipts" do
      receipt = receipt_fixture()
      assert Sales.list_receipts() == [receipt]
    end

    test "get_receipt!/1 returns the receipt with given id" do
      receipt = receipt_fixture()
      assert Sales.get_receipt!(receipt.id) == receipt
    end

    test "create_receipt/1 with valid data creates a receipt" do
      valid_attrs = %{date: ~U[2024-04-24 12:24:00Z], total_price: "120.5"}

      assert {:ok, %Receipt{} = receipt} = Sales.create_receipt(valid_attrs)
      assert receipt.date == ~U[2024-04-24 12:24:00Z]
      assert receipt.total_price == Decimal.new("120.5")
    end

    test "create_receipt/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_receipt(@invalid_attrs)
    end

    test "update_receipt/2 with valid data updates the receipt" do
      receipt = receipt_fixture()
      update_attrs = %{date: ~U[2024-04-25 12:24:00Z], total_price: "456.7"}

      assert {:ok, %Receipt{} = receipt} = Sales.update_receipt(receipt, update_attrs)
      assert receipt.date == ~U[2024-04-25 12:24:00Z]
      assert receipt.total_price == Decimal.new("456.7")
    end

    test "update_receipt/2 with invalid data returns error changeset" do
      receipt = receipt_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_receipt(receipt, @invalid_attrs)
      assert receipt == Sales.get_receipt!(receipt.id)
    end

    test "delete_receipt/1 deletes the receipt" do
      receipt = receipt_fixture()
      assert {:ok, %Receipt{}} = Sales.delete_receipt(receipt)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_receipt!(receipt.id) end
    end

    test "change_receipt/1 returns a receipt changeset" do
      receipt = receipt_fixture()
      assert %Ecto.Changeset{} = Sales.change_receipt(receipt)
    end
  end

  describe "pricelists" do
    alias KazRubdKr.Sales.Pricelist

    import KazRubdKr.SalesFixtures

    @invalid_attrs %{date: nil}

    test "list_pricelists/0 returns all pricelists" do
      pricelist = pricelist_fixture()
      assert Sales.list_pricelists() == [pricelist]
    end

    test "get_pricelist!/1 returns the pricelist with given id" do
      pricelist = pricelist_fixture()
      assert Sales.get_pricelist!(pricelist.id) == pricelist
    end

    test "create_pricelist/1 with valid data creates a pricelist" do
      valid_attrs = %{date: ~U[2024-04-24 12:35:00Z]}

      assert {:ok, %Pricelist{} = pricelist} = Sales.create_pricelist(valid_attrs)
      assert pricelist.date == ~U[2024-04-24 12:35:00Z]
    end

    test "create_pricelist/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_pricelist(@invalid_attrs)
    end

    test "update_pricelist/2 with valid data updates the pricelist" do
      pricelist = pricelist_fixture()
      update_attrs = %{date: ~U[2024-04-25 12:35:00Z]}

      assert {:ok, %Pricelist{} = pricelist} = Sales.update_pricelist(pricelist, update_attrs)
      assert pricelist.date == ~U[2024-04-25 12:35:00Z]
    end

    test "update_pricelist/2 with invalid data returns error changeset" do
      pricelist = pricelist_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_pricelist(pricelist, @invalid_attrs)
      assert pricelist == Sales.get_pricelist!(pricelist.id)
    end

    test "delete_pricelist/1 deletes the pricelist" do
      pricelist = pricelist_fixture()
      assert {:ok, %Pricelist{}} = Sales.delete_pricelist(pricelist)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_pricelist!(pricelist.id) end
    end

    test "change_pricelist/1 returns a pricelist changeset" do
      pricelist = pricelist_fixture()
      assert %Ecto.Changeset{} = Sales.change_pricelist(pricelist)
    end
  end
end
