defmodule KazRubdKrWeb.Receipt_printerLiveTest do
  use KazRubdKrWeb.ConnCase

  import Phoenix.LiveViewTest
  import KazRubdKr.SalesFixtures

  @create_attrs %{name: "some name", connection_details: "some connection_details"}
  @update_attrs %{name: "some updated name", connection_details: "some updated connection_details"}
  @invalid_attrs %{name: nil, connection_details: nil}

  defp create_receipt_printer(_) do
    receipt_printer = receipt_printer_fixture()
    %{receipt_printer: receipt_printer}
  end

  describe "Index" do
    setup [:create_receipt_printer]

    test "lists all receipt_printers", %{conn: conn, receipt_printer: receipt_printer} do
      {:ok, _index_live, html} = live(conn, ~p"/receipt_printers")

      assert html =~ "Listing Receipt printers"
      assert html =~ receipt_printer.name
    end

    test "saves new receipt_printer", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/receipt_printers")

      assert index_live |> element("a", "New Receipt printer") |> render_click() =~
               "New Receipt printer"

      assert_patch(index_live, ~p"/receipt_printers/new")

      assert index_live
             |> form("#receipt_printer-form", receipt_printer: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#receipt_printer-form", receipt_printer: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/receipt_printers")

      html = render(index_live)
      assert html =~ "Receipt printer created successfully"
      assert html =~ "some name"
    end

    test "updates receipt_printer in listing", %{conn: conn, receipt_printer: receipt_printer} do
      {:ok, index_live, _html} = live(conn, ~p"/receipt_printers")

      assert index_live |> element("#receipt_printers-#{receipt_printer.id} a", "Edit") |> render_click() =~
               "Edit Receipt printer"

      assert_patch(index_live, ~p"/receipt_printers/#{receipt_printer}/edit")

      assert index_live
             |> form("#receipt_printer-form", receipt_printer: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#receipt_printer-form", receipt_printer: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/receipt_printers")

      html = render(index_live)
      assert html =~ "Receipt printer updated successfully"
      assert html =~ "some updated name"
    end

    test "deletes receipt_printer in listing", %{conn: conn, receipt_printer: receipt_printer} do
      {:ok, index_live, _html} = live(conn, ~p"/receipt_printers")

      assert index_live |> element("#receipt_printers-#{receipt_printer.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#receipt_printers-#{receipt_printer.id}")
    end
  end

  describe "Show" do
    setup [:create_receipt_printer]

    test "displays receipt_printer", %{conn: conn, receipt_printer: receipt_printer} do
      {:ok, _show_live, html} = live(conn, ~p"/receipt_printers/#{receipt_printer}")

      assert html =~ "Show Receipt printer"
      assert html =~ receipt_printer.name
    end

    test "updates receipt_printer within modal", %{conn: conn, receipt_printer: receipt_printer} do
      {:ok, show_live, _html} = live(conn, ~p"/receipt_printers/#{receipt_printer}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Receipt printer"

      assert_patch(show_live, ~p"/receipt_printers/#{receipt_printer}/show/edit")

      assert show_live
             |> form("#receipt_printer-form", receipt_printer: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#receipt_printer-form", receipt_printer: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/receipt_printers/#{receipt_printer}")

      html = render(show_live)
      assert html =~ "Receipt printer updated successfully"
      assert html =~ "some updated name"
    end
  end
end
