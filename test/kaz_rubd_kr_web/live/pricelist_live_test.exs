defmodule KazRubdKrWeb.PricelistLiveTest do
  use KazRubdKrWeb.ConnCase

  import Phoenix.LiveViewTest
  import KazRubdKr.SalesFixtures

  @create_attrs %{date: "2024-04-24T12:35:00Z"}
  @update_attrs %{date: "2024-04-25T12:35:00Z"}
  @invalid_attrs %{date: nil}

  defp create_pricelist(_) do
    pricelist = pricelist_fixture()
    %{pricelist: pricelist}
  end

  describe "Index" do
    setup [:create_pricelist]

    test "lists all pricelists", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/pricelists")

      assert html =~ "Listing Pricelists"
    end

    test "saves new pricelist", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/pricelists")

      assert index_live |> element("a", "New Pricelist") |> render_click() =~
               "New Pricelist"

      assert_patch(index_live, ~p"/pricelists/new")

      assert index_live
             |> form("#pricelist-form", pricelist: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#pricelist-form", pricelist: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/pricelists")

      html = render(index_live)
      assert html =~ "Pricelist created successfully"
    end

    test "updates pricelist in listing", %{conn: conn, pricelist: pricelist} do
      {:ok, index_live, _html} = live(conn, ~p"/pricelists")

      assert index_live |> element("#pricelists-#{pricelist.id} a", "Edit") |> render_click() =~
               "Edit Pricelist"

      assert_patch(index_live, ~p"/pricelists/#{pricelist}/edit")

      assert index_live
             |> form("#pricelist-form", pricelist: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#pricelist-form", pricelist: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/pricelists")

      html = render(index_live)
      assert html =~ "Pricelist updated successfully"
    end

    test "deletes pricelist in listing", %{conn: conn, pricelist: pricelist} do
      {:ok, index_live, _html} = live(conn, ~p"/pricelists")

      assert index_live |> element("#pricelists-#{pricelist.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#pricelists-#{pricelist.id}")
    end
  end

  describe "Show" do
    setup [:create_pricelist]

    test "displays pricelist", %{conn: conn, pricelist: pricelist} do
      {:ok, _show_live, html} = live(conn, ~p"/pricelists/#{pricelist}")

      assert html =~ "Show Pricelist"
    end

    test "updates pricelist within modal", %{conn: conn, pricelist: pricelist} do
      {:ok, show_live, _html} = live(conn, ~p"/pricelists/#{pricelist}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Pricelist"

      assert_patch(show_live, ~p"/pricelists/#{pricelist}/show/edit")

      assert show_live
             |> form("#pricelist-form", pricelist: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#pricelist-form", pricelist: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/pricelists/#{pricelist}")

      html = render(show_live)
      assert html =~ "Pricelist updated successfully"
    end
  end
end
