defmodule KazRubdKrWeb.StaffLiveTest do
  use KazRubdKrWeb.ConnCase

  import Phoenix.LiveViewTest
  import KazRubdKr.SalesFixtures

  @create_attrs %{full_name: "some full_name"}
  @update_attrs %{full_name: "some updated full_name"}
  @invalid_attrs %{full_name: nil}

  defp create_staff(_) do
    staff = staff_fixture()
    %{staff: staff}
  end

  describe "Index" do
    setup [:create_staff]

    test "lists all staff", %{conn: conn, staff: staff} do
      {:ok, _index_live, html} = live(conn, ~p"/staff")

      assert html =~ "Listing Staff"
      assert html =~ staff.full_name
    end

    test "saves new staff", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/staff")

      assert index_live |> element("a", "New Staff") |> render_click() =~
               "New Staff"

      assert_patch(index_live, ~p"/staff/new")

      assert index_live
             |> form("#staff-form", staff: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#staff-form", staff: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/staff")

      html = render(index_live)
      assert html =~ "Staff created successfully"
      assert html =~ "some full_name"
    end

    test "updates staff in listing", %{conn: conn, staff: staff} do
      {:ok, index_live, _html} = live(conn, ~p"/staff")

      assert index_live |> element("#staff-#{staff.id} a", "Edit") |> render_click() =~
               "Edit Staff"

      assert_patch(index_live, ~p"/staff/#{staff}/edit")

      assert index_live
             |> form("#staff-form", staff: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#staff-form", staff: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/staff")

      html = render(index_live)
      assert html =~ "Staff updated successfully"
      assert html =~ "some updated full_name"
    end

    test "deletes staff in listing", %{conn: conn, staff: staff} do
      {:ok, index_live, _html} = live(conn, ~p"/staff")

      assert index_live |> element("#staff-#{staff.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#staff-#{staff.id}")
    end
  end

  describe "Show" do
    setup [:create_staff]

    test "displays staff", %{conn: conn, staff: staff} do
      {:ok, _show_live, html} = live(conn, ~p"/staff/#{staff}")

      assert html =~ "Show Staff"
      assert html =~ staff.full_name
    end

    test "updates staff within modal", %{conn: conn, staff: staff} do
      {:ok, show_live, _html} = live(conn, ~p"/staff/#{staff}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Staff"

      assert_patch(show_live, ~p"/staff/#{staff}/show/edit")

      assert show_live
             |> form("#staff-form", staff: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#staff-form", staff: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/staff/#{staff}")

      html = render(show_live)
      assert html =~ "Staff updated successfully"
      assert html =~ "some updated full_name"
    end
  end
end
